package my.crud.controller;

import my.crud.model.Message;
import my.crud.model.Role;
import my.crud.model.User;
import my.crud.service.MessageService;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;

@Controller
public class MainController {
    private final MessageService messageService;

    public MainController(MessageService messageService) {
        this.messageService = messageService;
    }

    @GetMapping("/")
    public String greeting() {
        return "greetings/greeting";
    }

    @GetMapping("/main")
    public String main(@AuthenticationPrincipal User user, Model model) {
        model.addAttribute("user", user);
        ArrayList<String> roles = new ArrayList<>();
        for (Role role : user.getRoles()) {
            roles.add(role.name());
        }
        model.addAttribute("roles", roles);
        model.addAttribute("messages", messageService.findActive());
        return "/main/main";
    }

    @PostMapping("/main")
    public String add(@AuthenticationPrincipal User user, @RequestParam String text, Model model) {
        messageService.save(new Message(text, user));
        return "redirect:/main";
    }
}