package my.crud.controller;

import my.crud.model.Message;
import my.crud.model.Role;
import my.crud.model.User;
import my.crud.service.MessageService;
import my.crud.service.UserService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
@RequestMapping("/admin")
@PreAuthorize("hasAuthority('ADMIN')")
public class AdminController {
    private final UserService userService;

    private final MessageService messageService;

    public AdminController(UserService userService, MessageService messageService) {
        this.userService = userService;
        this.messageService = messageService;
    }

    @GetMapping
    public String userList(Model model) {
        model.addAttribute("users", userService.findActive());
        return "admin/userList";
    }

    @GetMapping("/editUser")
    public String editUser(@RequestParam("id") User user, Model model) {
        model.addAttribute("user", user);
        model.addAttribute("allRoles", Role.values());
        return "admin/userEdit";
    }

    @PostMapping("/editUser")
    public String saveUser(@RequestParam("userId") User user,
                           @RequestParam("roles") List<String> roles) {
        user.getRoles().clear();
        for (String role : roles) {
            user.getRoles().add(Role.valueOf(role));
        }
        userService.save(user);
        return "redirect:/admin";
    }

    @PostMapping("/deleteUser")
    public String deleteUser(@RequestParam("id") User user) {
        Iterable<Message> messages = messageService.findAll();
        for (Message message : messages) {
            if (message.getUserId().equals(user)) {
                message.setIsDeleted(true);
                messageService.save(message);
            }
        }
        user.setDeleted(true);
        userService.save(user);
        return "redirect:/admin";
    }
}
