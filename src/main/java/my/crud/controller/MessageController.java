package my.crud.controller;

import my.crud.model.Message;
import my.crud.service.MessageService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class MessageController {
    private final MessageService messageService;

    public MessageController(MessageService messageService) {
        this.messageService = messageService;
    }

    @GetMapping("/editMessage")
    public String edit(@RequestParam("id") Message message, Model model) {
        model.addAttribute("message", message);
        return "message/editMessage";
    }

    @PostMapping("/editMessage")
    public String editMessage(@RequestParam("id") Message message, @RequestParam String text) {
        message.setText(text);
        messageService.save(message);
        return "redirect:/main";
    }

    @PostMapping("/removeMessage")
    public String removeMessage(@RequestParam("id") Message message) {
        message.setIsDeleted(true);
        messageService.save(message);
        return "redirect:/main";
    }
}
