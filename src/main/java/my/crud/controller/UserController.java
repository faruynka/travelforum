package my.crud.controller;

import my.crud.model.User;
import my.crud.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/user")
public class UserController {
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/settings")
    public String settings(@RequestParam("id") User user, Model model) {
        model.addAttribute("user", user);
        return "user/settings";
    }

    @PostMapping("/settings")
    public String changePassword(@RequestParam("id") User user,
                                 @RequestParam("currentPassword") String currentPassword,
                                 @RequestParam("newPassword") String newPassword) {
        if (!user.getPassword().equals(currentPassword)) {
            return "redirect:/errors/errorPassword";
        }
        user.setPassword(newPassword);
        userService.save(user);
        return "redirect:/main";
    }
}
