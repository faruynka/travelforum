package my.crud.service;

import my.crud.model.User;
import my.crud.repo.UserRepo;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserService implements UserDetailsService {
    private final UserRepo userRepo;

    public UserService(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepo.findByUsername(username);
    }

    public User findByUsername(String username) {
        return userRepo.findByUsername(username);
    }

    public User save(User user) {
        return userRepo.save(user);
    }

    public void delete(User user) {
        userRepo.delete(user);
    }

    public Iterable<User> findAll() {
        return userRepo.findAll();
    }

    public List<User> findActive() {
        List<User> activeUsers = new ArrayList<>();
        for (User user: findAll()) {
            if (!user.getDeleted()) {
                activeUsers.add(user);
            }
        }
        return activeUsers;
    }
}
