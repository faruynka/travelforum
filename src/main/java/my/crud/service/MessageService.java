package my.crud.service;

import my.crud.model.Message;
import my.crud.model.User;
import my.crud.repo.MessageRepo;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MessageService {
    private final MessageRepo messageRepo;

    public MessageService(MessageRepo messageRepo) {
        this.messageRepo = messageRepo;
    }

    public Long deleteByUserId(User user) {
        return messageRepo.deleteByUserId(user);
    }

    public Message save(Message message) {
        return messageRepo.save(message);
    }

    public void delete(Message message) {
        messageRepo.delete(message);
    }

    public Iterable<Message> findAll() {
        return messageRepo.findAll();
    }

    public List<Message> findActive() {
        List<Message> activeMessages = new ArrayList<>();
        for (Message message: findAll()) {
            if (!message.getIsDeleted()) {
                activeMessages.add(message);
            }
        }
        return activeMessages;
    }

}
