package my.crud.repo;

import my.crud.model.Message;
import my.crud.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

public interface MessageRepo extends CrudRepository<Message, Long> {
    @Transactional
    Long deleteByUserId(User user);
}
