package my.crud.sheduler.job;

import my.crud.model.User;
import my.crud.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class DeleteUserTask {
    @Autowired
    UserService userService;

    @Scheduled(cron = "0 0 0 ? * 2/7 *")
    public void deleteUsers() {
        Iterable<User> users = userService.findAll();
        for (User user : users) {
            if (user.getDeleted()) {
                userService.delete(user);
            }
        }
    }
}
