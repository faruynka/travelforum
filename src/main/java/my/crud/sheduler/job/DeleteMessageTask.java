package my.crud.sheduler.job;

import my.crud.model.Message;
import my.crud.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class DeleteMessageTask {
    @Autowired
    MessageService messageService;

    @Scheduled(cron = "0 0 0 ? * 2/7 *")
    public void deleteMessages() {
        Iterable<Message> messages = messageService.findAll();
        for (Message message : messages) {
            if (message.getIsDeleted()) {
                messageService.delete(message);
            }
        }
    }
}
