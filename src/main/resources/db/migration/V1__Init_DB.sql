create sequence hibernate_sequence start 1 increment 1;
create table message (
    id         int8 not null,
    text       varchar(2048) not null,
    updated_at timestamp,
    user_id    int8,
    primary key(id)
);

create table user_role(
  user_id int8 not null,
  roles   varchar(255)
);

create table users (
    id       int8    not null,
    username varchar(255) not null,
    password varchar(255) not null,
    active   boolean not null,
    primary key(id)
);

alter table message
add constraint message_user_fk foreign key(user_id)references users;
alter table user_role
add constraint user_role_user_fk foreign key(user_id)references users;