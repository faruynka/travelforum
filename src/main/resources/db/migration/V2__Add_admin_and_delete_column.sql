alter table users
ADD COLUMN is_deleted BOOLEAN;

ALTER TABLE message
ADD COLUMN is_deleted BOOLEAN;

insert into users (id, username, password, active, is_deleted)
    values (1, 'user', 'password', true, false);

insert into user_role(user_id, roles)
    values (1, 'USER'), (1, 'ADMIN');